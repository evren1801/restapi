package com.java.project.springBootApiUsers.Business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.java.project.springBootApiUsers.DataAccess.IUsersDal;
import com.java.project.springBootApiUsers.Entities.Users;

@Service
public class UsersManager implements IUsersService{

	private IUsersDal usersDal;

	@Autowired
	public UsersManager(IUsersDal usersDal) {
		this.usersDal = usersDal;
	}

	@Override
	public List<Users> getAll() {
		return this.usersDal.getAll();
	}

	@Override
	public void add(Users users) {
		this.usersDal.add(users);
		
	}

	@Override
	public void update(Users users) {
		this.usersDal.update(users);
		
	}

	@Override
	public void delete(Users users) {
		this.usersDal.delete(users);
		
	}

}
