package com.java.project.springBootApiUsers.Business;

import java.util.List;

import com.java.project.springBootApiUsers.Entities.Users;

public interface IUsersService {

	List<Users> getAll();
	void add(Users users);
	void update(Users users);
	void delete(Users users);
	
}
