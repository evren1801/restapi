package com.java.project.springBootApiUsers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootApiUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApiUsersApplication.class, args);		
	}

}
