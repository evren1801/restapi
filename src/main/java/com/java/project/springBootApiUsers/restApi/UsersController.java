package com.java.project.springBootApiUsers.restApi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.java.project.springBootApiUsers.Business.IUsersService;
import com.java.project.springBootApiUsers.Entities.Users;

@RestController
@RequestMapping("/users")
public class UsersController {

	private IUsersService usersService;
	
	@Autowired
	public UsersController(IUsersService usersService) {

		this.usersService = usersService;
	}

	@GetMapping("/get")
	public List<Users> get() {
		return this.usersService.getAll();
	}

	@PostMapping("/add")
	public void add(@RequestBody Users users) {
		this.usersService.add(users);
		
	}

	@PostMapping("/update")
	public void update(@RequestBody Users users) {
		this.usersService.update(users);
		
	}

	@PostMapping("/delete")
	public void delete(@RequestBody Users users) {
		this.usersService.delete(users);
		
	}

}
