package com.java.project.springBootApiUsers.DataAccess;
import java.util.List;

import com.java.project.springBootApiUsers.Entities.*;

public interface IUsersDal {
	
	List<Users> getAll();
	void add(Users users);
	void update(Users users);
	void delete(Users users);

}
