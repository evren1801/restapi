package com.java.project.springBootApiUsers.DataAccess;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.java.project.springBootApiUsers.Entities.Users;

@Repository
public class HibernateUsersDal implements IUsersDal{

	private EntityManager entityManager;
	
	@Autowired
	public HibernateUsersDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional
	public List<Users> getAll() {
		Session session = entityManager.unwrap(Session.class);
		List<Users> allUsers = session.createQuery("from Users", Users.class).getResultList();
		return allUsers;
	}

	@Override
	@Transactional
	public void add(Users users) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(users);
	}

	@Override
	@Transactional
	public void update(Users users) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(users);
	}

	@Override
	@Transactional
	public void delete(Users users) {
		Session session = entityManager.unwrap(Session.class);
		Users deleteUser = session.get(Users.class, users.getId());
		session.delete(deleteUser);
	}

}
